package me.clip.deluxetags.database;
import java.util.*;
import java.util.logging.Level;

import com.google.common.collect.Lists;

import java.sql.*;

public class Database {
    public static Database i;

    static {
        Database.i = new Database();
    }

    public static Database getData() {
        return Database.i;
    }

    public void createTable(final String name, final List<String> integers, final List<String> strings) {
        String integerliste = "";
        for (final String inte : integers) {
            integerliste = String.valueOf(integerliste) + ", " + inte + " INT(255)";
        }
        String stringliste = "";
        for (final String string : strings) {
            stringliste = String.valueOf(stringliste) + ", " + string + " VARCHAR(255)";
        }
        MySQL.Update("CREATE TABLE IF NOT EXISTS " + name + " (id INT AUTO_INCREMENT PRIMARY KEY" + stringliste + integerliste + ")");
    }

    public void setInt(final String tablename, final String column, final String where, final String what, final int integer) {
        MySQL.Update("UPDATE " + tablename + " SET " + column + "= '" + integer + "' WHERE " + where + "= '" + what + "';");
    }

    public void setString(final String tablename, final String path, final String where, final String what, final String integer) {
        MySQL.Update("UPDATE " + tablename + " SET " + path + "= '" + integer + "' WHERE " + where + "= '" + what + "';");
    }

    public String getString(final String tablename, final String column, final String where, final String what) {
        String string = "";
        try {
            final ResultSet rs = MySQL.Query("SELECT " + column + " FROM " + tablename + " WHERE " + where + "= '" + what + "';");
            while (rs.next()) {
                string = rs.getString(1);
            }
            rs.close();
        }
        catch (Exception e) {
            System.err.println(e);
        }
        return string;
    }

    public Integer getInt(final String tableName, final String column, final String where, final String what) {
        int integer = 0;
        try {
            final ResultSet rs = MySQL.Query("SELECT " + column + " FROM " + tableName + " WHERE " + where + "= '" + what + "';");
            while (rs.next()) {
                integer = rs.getInt(1);
            }
            rs.close();
        }
        catch (Exception e) {
            System.err.println(e);
        }
        return integer;
    }

    public int countRowsInTable(String tableName){
        try{
            PreparedStatement sql = MySQL.con.prepareStatement("SELECT COUNT(*) FROM `" + tableName + "`;");
            ResultSet rs = sql.executeQuery();
            rs.first();
            int numberOfRows = rs.getInt("COUNT(*)");

            sql.close();

            return numberOfRows;
        }catch (Exception e){
            e.printStackTrace();
            return 0;
        }
    }
}