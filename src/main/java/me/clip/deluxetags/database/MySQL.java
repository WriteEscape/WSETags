package me.clip.deluxetags.database;
import java.sql.*;

public class MySQL
{
    public static Connection con;

    public static void connect(final String host, final String db, final String user, final String password) {
        try {
            MySQL.con = DriverManager.getConnection("jdbc:mysql://" + host + ":3306/" + db + "?autoReconnect=true", user, password);
            System.out.println("[MySQL] Connect.");
        }
        catch (SQLException e) {
            e.printStackTrace();
            System.out.println("[MySQL] Connect failed.");
        }
    }

    public static void close() {
        if (MySQL.con != null) {
            try {
                MySQL.con.close();
            }
            catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static void Update(final String qry) {
        try {
            final Statement stmt = MySQL.con.createStatement();
            stmt.executeUpdate(qry);
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static ResultSet Query(final String qry) {
        ResultSet rs = null;
        try {
            final Statement stmt = MySQL.con.createStatement();
            rs = stmt.executeQuery(qry);
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return rs;
    }
}

