package me.clip.deluxetags.database;
import me.clip.deluxetags.DeluxeTag;
import me.clip.deluxetags.DeluxeTags;
import me.clip.deluxetags.TagConfig;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.Set;

import me.clip.deluxetags.config.ConfigWrapper;
import org.bukkit.configuration.file.FileConfiguration;


public class CompareDatabase {
    DeluxeTags plugin;
    FileConfiguration config;

    //makes it so we're able to get config.yml
    public CompareDatabase(DeluxeTags i) {
        this.plugin = i;
        this.config = this.plugin.getConfig();
    }

    // method to check if tags in config.yml also exist in SQL database
    public boolean checkTags() throws SQLException { // will return true if all tags exist in both places, false if not
        FileConfiguration c = this.plugin.getConfig();
        boolean difference;

        Set<String> keys = c.getConfigurationSection("deluxetags").getKeys(false); // gets keys stuff, idk, copied from TagConfig

        if (keys != null && !keys.isEmpty()) {
            Iterator<String> it = keys.iterator();
            ResultSet rs = MySQL.Query("SELECT 'identifier' FROM 'ranks'"); // gets all identifiers from database

            while (it.hasNext()) { // goes through all identifiers in config.yml
                String identifier = it.next(); // current identifier from config.yml

                difference = true;
                while (rs.next()) { // goes through all identifiers from database
                    if (identifier != null && identifier.equals(rs.getString(1))) { // checks if tag from config.yml exists in database
                        difference = false;
                    }
                }

                if (difference) { // runs if the current tag didn't exist in the database
                    return false;
                }
            }
            return true; // woo if the code got all the way to here, all tags must've been in both places
        }
        return false; // should only get here if there are no keys, but if there are no keys, something else is wrong with config.yml lol
    }

    // to update config.yml if there are less tags in it than in the database
    public void updateConfig() throws SQLException {
        FileConfiguration c = this.plugin.getConfig();
        Set<String> keys = c.getConfigurationSection("deluxetags").getKeys(false);
        ResultSet rs = MySQL.Query("SELECT * FROM 'ranks'"); // get the entire ranks table
        int numOfTagsConfig = keys.size();
        int numOfTagsSQL = Database.getData().countRowsInTable("ranks");


        if (numOfTagsConfig < numOfTagsSQL) {
            Iterator<String> it = keys.iterator();

//            c.set("deluxetags.*", null); // I'm not sure that this actually deletes all tags. If it doesn't, use the commented things instead
//            this.plugin.saveConfig();

            while (it.hasNext()) {
                String identifier = it.next();
                this.plugin.getCfg().removeTag(identifier);
            }

            while (rs.next()) { // go through all tags in the database and add them to config.yml
                String identifier = rs.getString("identifier");
                int priority = rs.getInt("priority");
                String displayTag = rs.getString("displayTag");
                String description = rs.getString("description");
                String permission = rs.getString("identifier");

                // doing the same as when deluxetags add their own new tags
                DeluxeTag dTag = new DeluxeTag(priority, identifier, displayTag, description);
                dTag.load();

                this.plugin.getCfg().saveTag(priority, identifier, displayTag, description, permission);
            }
        }
    }

    // to update player_tags.yml from database
    public void updatePlayerFile() throws SQLException {
        FileConfiguration c = this.plugin.getPlayerFile().getConfig();
        ResultSet rs = MySQL.Query("SELECT * FROM 'players'"); // get the entire player table

        while (rs.next()) {
            c.set(rs.getString("uuid"), rs.getString("tag"));
            this.plugin.savePlayerFile();
        }

    }

    // to be called when someone changes their tag. It will update the database. I don't know where to put it though
    public void updatePlayerDB(String uuid, String tag) throws SQLException {
        ResultSet rs = MySQL.Query("SELECT EXISTS(SELECT * FROM 'players' WHERE 'uuid'='" + uuid + "'"); // to check whether or not said uuid already exists in the database
        rs.next();
        if (rs.getBoolean(1)) { // should get the value... correct me if I'm wrong
            MySQL.Update("UPDATE 'players' SET 'tag'='" + tag + "' WHERE 'uuid'='" + uuid + "'"); // update tag if uuid exists
        } else {
            MySQL.Query("INSERT INTO 'players' ('uuid','tag') VALUES ('" + uuid + "','" + tag + "')"); // create new player if they don't exist
        }
    }

    // to be called when a new tag is made or changed. It will update the database. Also don't know where to put this in the plugin's code
    public void updateTagsDB(String identifier, int priority, String displayTag, String description, String permission) throws SQLException {
        ResultSet rs = MySQL.Query("SELECT EXISTS(SELECT * FROM 'ranks' WHERE 'identifier'='" + identifier + "'"); // to check whether or not said tag already exists in the database
        rs.next();
        if (rs.getBoolean(1)) { // should get the value... correct me if I'm wrong
            MySQL.Update("UPDATE 'ranks' SET 'priority'='" + priority + "', 'displayTag'='" + displayTag + "', 'description'='" + description + "', 'permission'='" + permission + "' WHERE 'identifier'='" + identifier + "'"); // update tag if it exists
        } else {
            MySQL.Query("INSERT INTO 'ranks' ('identifier','priority','displayTag','description','permission') VALUES ('" + identifier + "','" + priority + "','" + displayTag + "','" + description + "','" + permission + "')"); // create new tag if it doesn't exist
        }
    }

}
