/*     */ package me.clip.deluxetags.commands;
/*     */ 
/*     */ import java.util.Arrays;
/*     */ import java.util.Collection;
/*     */ import java.util.List;
/*     */ import me.clip.deluxetags.DeluxeTag;
/*     */ import me.clip.deluxetags.DeluxeTags;
/*     */ import me.clip.deluxetags.Lang;
/*     */ import me.clip.deluxetags.utils.MsgUtils;
/*     */ import org.apache.commons.lang.StringUtils;
/*     */ import org.bukkit.Bukkit;
/*     */ import org.bukkit.command.Command;
/*     */ import org.bukkit.command.CommandExecutor;
/*     */ import org.bukkit.command.CommandSender;
/*     */ import org.bukkit.entity.Player;
/*     */ 
/*     */ public class TagCommand
/*     */   implements CommandExecutor {
/*     */   private DeluxeTags plugin;
/*     */   
/*     */   public TagCommand(DeluxeTags i) {
/*  22 */     this.plugin = i;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public boolean onCommand(CommandSender s, Command c, String label, String[] args) {
/*  29 */     if (args.length == 0) {
/*     */       
/*  31 */       if (!(s instanceof Player)) {
/*  32 */         MsgUtils.msg(s, "&8&m+----------------+");
/*  33 */         MsgUtils.msg(s, "&5&lDeluxeTags &f&o" + this.plugin.getDescription().getVersion());
/*  34 */         MsgUtils.msg(s, "&7Created by &f&oextended_clip");
/*  35 */         MsgUtils.msg(s, "Use /tags help for console commands");
/*  36 */         MsgUtils.msg(s, "&8&m+----------------+");
/*  37 */         return true;
/*     */       } 
/*     */       
/*  40 */       if (!s.hasPermission("deluxetags.gui")) {
/*  41 */         MsgUtils.msg(s, Lang.CMD_NO_PERMS.getConfigValue(new String[] { "deluxetags.gui" }));
/*     */ 
/*     */         
/*  44 */         return true;
/*     */       } 
/*     */       
/*  47 */       if (DeluxeTag.getLoadedTags() == null || DeluxeTag.getLoadedTags().isEmpty()) {
/*  48 */         MsgUtils.msg(s, Lang.CMD_NO_TAGS_LOADED.getConfigValue(null));
/*  49 */         return true;
/*     */       } 
/*     */       
/*  52 */       Player p = (Player)s;
/*     */       
/*  54 */       if (!this.plugin.getGUIHandler().openMenu(p, 1)) {
/*  55 */         MsgUtils.msg(s, Lang.CMD_NO_TAGS_AVAILABLE.getConfigValue(null));
/*     */       }
/*  57 */       return true;
/*     */     } 
/*  59 */     if (args[0].equalsIgnoreCase("help")) {
/*     */       
/*  61 */       String color = Lang.CMD_HELP_COLOR.getConfigValue(null);
/*  62 */       MsgUtils.msg(s, "&8&m+----------------+");
/*  63 */       MsgUtils.msg(s, Lang.CMD_HELP_TITLE.getConfigValue(null));
/*  64 */       MsgUtils.msg(s, " ");
/*  65 */       if (s.hasPermission("deluxetags.gui")) {
/*  66 */         MsgUtils.msg(s, color + "/tags");
/*  67 */         MsgUtils.msg(s, Lang.CMD_HELP_TAGS.getConfigValue(null));
/*     */       } 
/*  69 */       if (s.hasPermission("deluxetags.list")) {
/*  70 */         MsgUtils.msg(s, color + "/tags list (all/<playername>)");
/*  71 */         MsgUtils.msg(s, Lang.CMD_HELP_LIST.getConfigValue(null));
/*     */       } 
/*  73 */       if (s.hasPermission("deluxetags.select")) {
/*  74 */         MsgUtils.msg(s, color + "/tags select <tag>");
/*  75 */         MsgUtils.msg(s, Lang.CMD_HELP_SELECT.getConfigValue(null));
/*     */       } 
/*  77 */       if (s.hasPermission("deluxetags.set")) {
/*  78 */         MsgUtils.msg(s, color + "/tags set <player> <tag>");
/*  79 */         MsgUtils.msg(s, Lang.CMD_HELP_ADMIN_SET.getConfigValue(null));
/*     */       } 
/*  81 */       if (s.hasPermission("deluxetags.clear")) {
/*  82 */         MsgUtils.msg(s, color + "/tags clear <player>");
/*  83 */         MsgUtils.msg(s, Lang.CMD_HELP_ADMIN_CLEAR.getConfigValue(null));
/*     */       } 
/*  85 */       if (s.hasPermission("deluxetags.create")) {
/*  86 */         MsgUtils.msg(s, color + "/tags create <identifier> <tag>");
/*  87 */         MsgUtils.msg(s, Lang.CMD_HELP_ADMIN_CREATE.getConfigValue(null));
/*     */       } 
/*  89 */       if (s.hasPermission("deluxetags.delete")) {
/*  90 */         MsgUtils.msg(s, color + "/tags delete <identifier>");
/*  91 */         MsgUtils.msg(s, Lang.CMD_HELP_ADMIN_DELETE.getConfigValue(null));
/*     */       } 
/*  93 */       if (s.hasPermission("deluxetags.setdescription")) {
/*  94 */         MsgUtils.msg(s, color + "/tags setdesc <identifier> <tag description>");
/*  95 */         MsgUtils.msg(s, Lang.CMD_HELP_ADMIN_SET_DESC.getConfigValue(null));
/*     */       } 
/*  97 */       if (s.hasPermission("deluxetags.reload")) {
/*  98 */         MsgUtils.msg(s, color + "/tags reload");
/*  99 */         MsgUtils.msg(s, Lang.CMD_HELP_RELOAD.getConfigValue(null));
/*     */       } 
/* 101 */       MsgUtils.msg(s, color + "/tags version");
/* 102 */       MsgUtils.msg(s, Lang.CMD_HELP_VERSION.getConfigValue(null));
/* 103 */       MsgUtils.msg(s, "&8&m+----------------+");
/* 104 */       return true;
/*     */     } 
/* 106 */     if (args[0].equalsIgnoreCase("version")) {
/*     */       
/* 108 */       MsgUtils.msg(s, "&8&m+----------------+");
/* 109 */       MsgUtils.msg(s, "&5&lDeluxeTags &f&o" + this.plugin.getDescription().getVersion());
/* 110 */       MsgUtils.msg(s, "&7Created by &f&oextended_clip");
/* 111 */       MsgUtils.msg(s, "&8&m+----------------+");
/*     */       
/* 113 */       return true;
/*     */     } 
/* 115 */     if (args[0].equalsIgnoreCase("list")) {
/*     */       
/* 117 */       if (args.length == 1) {
/*     */         List<String> t;
/* 119 */         if (!s.hasPermission("deluxetags.list")) {
/*     */           
/* 121 */           MsgUtils.msg(s, Lang.CMD_NO_PERMS.getConfigValue(new String[] { "deluxetags.list" }));
/*     */ 
/*     */           
/* 124 */           return true;
/*     */         } 
/*     */         
/* 127 */         if (DeluxeTag.getLoadedTags() == null || DeluxeTag.getLoadedTags().isEmpty()) {
/* 128 */           MsgUtils.msg(s, Lang.CMD_NO_TAGS_LOADED.getConfigValue(null));
/* 129 */           return true;
/*     */         } 
/*     */ 
/*     */ 
/*     */         
/* 134 */         if (!(s instanceof Player)) {
/* 135 */           t = DeluxeTag.getAllTagIdentifiers();
/*     */         } else {
/* 137 */           t = DeluxeTag.getAvailableTagIdentifiers((Player)s);
/*     */         } 
/*     */         
/* 140 */         String tags = t.toString().replace("[", "&7").replace(",", "&a,&7").replace("]", "");
/* 141 */         String amount = String.valueOf(t.size());
/* 142 */         MsgUtils.msg(s, Lang.CMD_TAG_LIST.getConfigValue(new String[] { amount, tags }));
/*     */       } else {
/*     */         
/* 145 */         if (args[1].equalsIgnoreCase("all")) {
/*     */           
/* 147 */           if (!s.hasPermission("deluxetags.list.all")) {
/*     */             
/* 149 */             MsgUtils.msg(s, Lang.CMD_NO_PERMS.getConfigValue(new String[] { "deluxetags.list.all" }));
/*     */ 
/*     */             
/* 152 */             return true;
/*     */           } 
/*     */           
/* 155 */           if (DeluxeTag.getLoadedTags() == null || DeluxeTag.getLoadedTags().isEmpty()) {
/*     */             
/* 157 */             MsgUtils.msg(s, Lang.CMD_NO_TAGS_LOADED.getConfigValue(null));
/* 158 */             return true;
/*     */           } 
/*     */           
/* 161 */           Collection<DeluxeTag> collection = DeluxeTag.getLoadedTags();
/*     */           
/* 163 */           StringBuilder sb = new StringBuilder();
/*     */           
/* 165 */           for (DeluxeTag d : collection) {
/* 166 */             sb.append("&f").append(d.getIdentifier()).append("&7:&f").append(d.getDisplayTag())
/* 167 */               .append("&a, ");
/*     */           }
/*     */           
/* 170 */           String str1 = sb.toString().trim();
/*     */           
/* 172 */           String str2 = String.valueOf(collection.size());
/*     */           
/* 174 */           MsgUtils.msg(s, Lang.CMD_TAG_LIST_ALL.getConfigValue(new String[] { str2, str1 }));
/*     */ 
/*     */ 
/*     */           
/* 178 */           return true;
/*     */         } 
/*     */ 
/*     */         
/* 182 */         if (!s.hasPermission("deluxetags.list.player")) {
/*     */           
/* 184 */           MsgUtils.msg(s, Lang.CMD_NO_PERMS.getConfigValue(new String[] { "deluxetags.list.player" }));
/*     */ 
/*     */           
/* 187 */           return true;
/*     */         } 
/*     */         
/* 190 */         if (DeluxeTag.getLoadedTags() == null || DeluxeTag.getLoadedTags().isEmpty()) {
/*     */           
/* 192 */           MsgUtils.msg(s, Lang.CMD_NO_TAGS_LOADED.getConfigValue(null));
/* 193 */           return true;
/*     */         } 
/*     */         
/* 196 */         String tar = args[1];
/*     */         
/* 198 */         Player target = Bukkit.getPlayer(tar);
/*     */         
/* 200 */         if (target == null) {
/*     */           
/* 202 */           MsgUtils.msg(s, Lang.CMD_TARGET_NOT_ONLINE.getConfigValue(new String[] { tar }));
/*     */ 
/*     */           
/* 205 */           return true;
/*     */         } 
/*     */         
/* 208 */         List<String> t = DeluxeTag.getAvailableTagIdentifiers(target);
/*     */         
/* 210 */         String tags = t.toString().replace("[", "&7").replace(",", "&a,&7").replace("]", "");
/*     */         
/* 212 */         String amount = String.valueOf(t.size());
/*     */         
/* 214 */         MsgUtils.msg(s, Lang.CMD_TAG_LIST_TARGET.getConfigValue(new String[] { target
/* 215 */                 .getName(), amount, tags }));
/*     */       } 
/*     */ 
/*     */       
/* 219 */       return true;
/*     */     } 
/* 221 */     if (args[0].equalsIgnoreCase("select")) {
/*     */       
/* 223 */       if (!(s instanceof Player)) {
/* 224 */         MsgUtils.msg(s, "&4This command can only be used in game!");
/* 225 */         return true;
/*     */       } 
/*     */       
/* 228 */       Player p = (Player)s;
/*     */       
/* 230 */       if (!s.hasPermission("deluxetags.select")) {
/* 231 */         MsgUtils.msg(s, Lang.CMD_NO_PERMS.getConfigValue(new String[] { "deluxetags.select" }));
/*     */ 
/*     */         
/* 234 */         return true;
/*     */       } 
/*     */       
/* 237 */       if (args.length != 2) {
/*     */         
/* 239 */         MsgUtils.msg(s, Lang.CMD_TAG_SEL_INCORRECT.getConfigValue(null));
/* 240 */         return true;
/*     */       } 
/*     */       
/* 243 */       if (DeluxeTag.getLoadedTags() == null || DeluxeTag.getLoadedTags().isEmpty()) {
/*     */         
/* 245 */         MsgUtils.msg(s, Lang.CMD_NO_TAGS_LOADED.getConfigValue(null));
/* 246 */         return true;
/*     */       } 
/*     */       
/* 249 */       List<String> avail = DeluxeTag.getAvailableTagIdentifiers(p);
/*     */       
/* 251 */       if (avail == null || avail.isEmpty()) {
/*     */         
/* 253 */         MsgUtils.msg(s, Lang.CMD_NO_TAGS_AVAILABLE.getConfigValue(null));
/* 254 */         return true;
/*     */       } 
/*     */       
/* 257 */       String tag = args[1];
/*     */       
/* 259 */       for (String t : avail) {
/*     */         
/* 261 */         if (t.equalsIgnoreCase(tag)) {
/*     */           
/* 263 */           DeluxeTag set = DeluxeTag.getLoadedTag(t);
/*     */           
/* 265 */           if (set != null) {
/*     */             
/* 267 */             if (set.setPlayerTag(p)) {
/*     */               
/* 269 */               this.plugin.saveTagIdentifier(p.getUniqueId().toString(), set.getIdentifier());
/*     */               
/* 271 */               MsgUtils.msg(s, Lang.CMD_TAG_SEL_SUCCESS.getConfigValue(new String[] { set
/* 272 */                       .getIdentifier(), set.getDisplayTag() }));
/*     */             } else {
/* 274 */               MsgUtils.msg(s, Lang.CMD_TAG_SEL_FAIL_SAMETAG.getConfigValue(new String[] { set
/* 275 */                       .getIdentifier(), set.getDisplayTag() }));
/*     */             } 
/*     */             
/* 278 */             return true;
/*     */           } 
/*     */         } 
/*     */       } 
/*     */       
/* 283 */       MsgUtils.msg(s, Lang.CMD_TAG_SEL_FAIL_INVALID.getConfigValue(new String[] { tag }));
/*     */ 
/*     */       
/* 286 */       return true;
/*     */     } 
/* 288 */     if (args[0].equalsIgnoreCase("create")) {
/*     */       
/* 290 */       if (!s.hasPermission("deluxetags.create")) {
/*     */         
/* 292 */         MsgUtils.msg(s, Lang.CMD_NO_PERMS.getConfigValue(new String[] { "deluxetags.create" }));
/*     */ 
/*     */         
/* 295 */         return true;
/*     */       } 
/*     */       
/* 298 */       if (args.length < 3) {
/*     */         
/* 300 */         MsgUtils.msg(s, Lang.CMD_ADMIN_CREATE_TAG_INCORRECT.getConfigValue(null));
/* 301 */         return true;
/*     */       } 
/*     */       
/* 304 */       String id = args[1];
/*     */       
/* 306 */       if (DeluxeTag.getLoadedTag(id) != null) {
/* 307 */         MsgUtils.msg(s, Lang.CMD_ADMIN_CREATE_TAG_FAIL.getConfigValue(new String[] { id }));
/*     */ 
/*     */         
/* 310 */         return true;
/*     */       } 
/*     */       
/* 313 */       String tag = StringUtils.join(Arrays.copyOfRange((Object[])args, 2, args.length), " ");
/*     */       
/* 315 */       if (!tag.isEmpty()) {
/*     */         
/* 317 */         if (tag.endsWith("_")) {
/* 318 */           tag = tag.substring(0, tag.length() - 1) + " ";
/*     */         }
/*     */         
/* 321 */         int priority = DeluxeTag.getLoadedTagsAmount() + 1;
/*     */         
/* 323 */         DeluxeTag dTag = new DeluxeTag(priority, id, tag, "");
/*     */         
/* 325 */         dTag.load();
/*     */         
/* 327 */         this.plugin.getCfg().saveTag(priority, id, tag, "&f", "deluxetags.tag." + id);
/*     */         
/* 329 */         MsgUtils.msg(s, Lang.CMD_ADMIN_CREATE_TAG_SUCCESS.getConfigValue(new String[] { id, tag }));
/*     */       } 
/*     */ 
/*     */ 
/*     */ 
/*     */       
/* 335 */       return true;
/*     */     } 
/* 337 */     if (args[0].equalsIgnoreCase("delete")) {
/*     */       
/* 339 */       if (!s.hasPermission("deluxetags.delete")) {
/*     */         
/* 341 */         MsgUtils.msg(s, Lang.CMD_NO_PERMS.getConfigValue(new String[] { "deluxetags.delete" }));
/*     */ 
/*     */         
/* 344 */         return true;
/*     */       } 
/*     */       
/* 347 */       if (args.length != 2) {
/*     */         
/* 349 */         MsgUtils.msg(s, Lang.CMD_ADMIN_DELETE_TAG_INCORRECT.getConfigValue(null));
/* 350 */         return true;
/*     */       } 
/*     */       
/* 353 */       String id = args[1];
/*     */       
/* 355 */       DeluxeTag tag = DeluxeTag.getLoadedTag(id);
/*     */       
/* 357 */       if (tag != null) {
/*     */         
/* 359 */         List<String> r = tag.removeActivePlayers();
/*     */         
/* 361 */         if (r != null && !r.isEmpty())
/*     */         {
/* 363 */           this.plugin.removeSavedTags(r);
/*     */         }
/*     */         
/* 366 */         if (tag.unload()) {
/*     */           
/* 368 */           this.plugin.getCfg().removeTag(id);
/*     */           
/* 370 */           tag = null;
/*     */           
/* 372 */           MsgUtils.msg(s, Lang.CMD_ADMIN_DELETE_TAG_SUCCESS.getConfigValue(new String[] { id }));
/*     */ 
/*     */ 
/*     */           
/* 376 */           return true;
/*     */         } 
/*     */       } 
/*     */       
/* 380 */       MsgUtils.msg(s, Lang.CMD_ADMIN_DELETE_TAG_FAIL.getConfigValue(new String[] { id }));
/*     */ 
/*     */ 
/*     */       
/* 384 */       return true;
/*     */     } 
/* 386 */     if (args[0].equalsIgnoreCase("setdesc") || args[0].equalsIgnoreCase("setdescription")) {
/*     */       
/* 388 */       if (!s.hasPermission("deluxetags.setdescription")) {
/*     */         
/* 390 */         MsgUtils.msg(s, Lang.CMD_NO_PERMS.getConfigValue(new String[] { "deluxetags.setdescription" }));
/*     */ 
/*     */         
/* 393 */         return true;
/*     */       } 
/*     */       
/* 396 */       if (args.length < 3) {
/*     */         
/* 398 */         MsgUtils.msg(s, Lang.CMD_ADMIN_SET_DESCRIPTION_INCORRECT.getConfigValue(null));
/* 399 */         return true;
/*     */       } 
/*     */       
/* 402 */       String id = args[1];
/*     */       
/* 404 */       if (DeluxeTag.getLoadedTag(id) == null) {
/* 405 */         MsgUtils.msg(s, Lang.CMD_ADMIN_SET_DESCRIPTION_FAIL.getConfigValue(new String[] { id }));
/*     */ 
/*     */         
/* 408 */         return true;
/*     */       } 
/*     */       
/* 411 */       DeluxeTag tag = DeluxeTag.getLoadedTag(id);
/*     */       
/* 413 */       String desc = StringUtils.join(Arrays.copyOfRange((Object[])args, 2, args.length), " ");
/*     */       
/* 415 */       if (desc.endsWith("_")) {
/* 416 */         desc = desc.substring(0, desc.length() - 1) + " ";
/*     */       }
/*     */       
/* 419 */       tag.setDescription(desc);
/*     */       
/* 421 */       this.plugin.getCfg().saveTag(tag.getPriority(), tag.getIdentifier(), tag.getDisplayTag(), tag
/* 422 */           .getDescription(), tag.getPermission());
/*     */       
/* 424 */       MsgUtils.msg(s, Lang.CMD_ADMIN_SET_DESCRIPTION_SUCCESS.getConfigValue(new String[] { id, tag
/* 425 */               .getDisplayTag(), desc }));
/*     */ 
/*     */       
/* 428 */       return true;
/*     */     } 
/* 430 */     if (args[0].equalsIgnoreCase("set")) {
/*     */       
/* 432 */       if (!s.hasPermission("deluxetags.set")) {
/*     */         
/* 434 */         MsgUtils.msg(s, Lang.CMD_NO_PERMS.getConfigValue(new String[] { "deluxetags.set" }));
/*     */ 
/*     */         
/* 437 */         return true;
/*     */       } 
/*     */       
/* 440 */       if (args.length != 3) {
/*     */         
/* 442 */         MsgUtils.msg(s, Lang.CMD_ADMIN_SET_INCORRECT_ARGS.getConfigValue(null));
/* 443 */         return true;
/*     */       } 
/*     */       
/* 446 */       if (DeluxeTag.getLoadedTags() == null || DeluxeTag.getLoadedTags().isEmpty()) {
/*     */         
/* 448 */         MsgUtils.msg(s, Lang.CMD_NO_TAGS_LOADED.getConfigValue(null));
/* 449 */         return true;
/*     */       } 
/*     */       
/* 452 */       Player target = Bukkit.getPlayer(args[1]);
/*     */       
/* 454 */       if (target == null) {
/*     */         
/* 456 */         MsgUtils.msg(s, Lang.CMD_TARGET_NOT_ONLINE.getConfigValue(new String[] { args[1] }));
/*     */ 
/*     */         
/* 459 */         return true;
/*     */       } 
/*     */       
/* 462 */       List<String> avail = DeluxeTag.getAvailableTagIdentifiers(target);
/*     */       
/* 464 */       if (avail == null || avail.isEmpty()) {
/*     */         
/* 466 */         MsgUtils.msg(s, Lang.CMD_ADMIN_SET_NO_TAGS.getConfigValue(new String[] { target
/* 467 */                 .getName() }));
/*     */         
/* 469 */         return true;
/*     */       } 
/*     */       
/* 472 */       String tag = args[2];
/*     */       
/* 474 */       for (String t : avail) {
/*     */         
/* 476 */         if (t.equalsIgnoreCase(tag)) {
/*     */           
/* 478 */           DeluxeTag set = DeluxeTag.getLoadedTag(t);
/*     */           
/* 480 */           if (set != null) {
/*     */             
/* 482 */             set.setPlayerTag(target);
/*     */             
/* 484 */             this.plugin.saveTagIdentifier(target.getUniqueId().toString(), set.getIdentifier());
/*     */             
/* 486 */             MsgUtils.msg(s, Lang.CMD_ADMIN_SET_SUCCESS.getConfigValue(new String[] { target
/* 487 */                     .getName(), set.getIdentifier(), set.getDisplayTag() }));
/*     */ 
/*     */             
/* 490 */             if (target != s)
/*     */             {
/* 492 */               MsgUtils.msg((CommandSender)target, Lang.CMD_ADMIN_SET_SUCCESS_TARGET.getConfigValue(new String[] { set
/* 493 */                       .getIdentifier(), set.getDisplayTag(), s.getName() }));
/*     */             }
/*     */             
/* 496 */             return true;
/*     */           } 
/*     */         } 
/*     */       } 
/*     */       
/* 501 */       MsgUtils.msg(s, Lang.CMD_ADMIN_SET_FAIL.getConfigValue(new String[] { tag, target
/* 502 */               .getName() }));
/*     */       
/* 504 */       return true;
/*     */     } 
/* 506 */     if (args[0].equalsIgnoreCase("clear")) {
/*     */       
/* 508 */       if (!s.hasPermission("deluxetags.clear")) {
/*     */         
/* 510 */         MsgUtils.msg(s, Lang.CMD_NO_PERMS.getConfigValue(new String[] { "deluxetags.clear" }));
/*     */ 
/*     */         
/* 513 */         return true;
/*     */       } 
/*     */       
/* 516 */       if (args.length != 2) {
/*     */         
/* 518 */         MsgUtils.msg(s, Lang.CMD_ADMIN_CLEAR_INCORRECT_ARGS.getConfigValue(null));
/* 519 */         return true;
/*     */       } 
/*     */       
/* 522 */       if (DeluxeTag.getLoadedTags() == null || DeluxeTag.getLoadedTags().isEmpty()) {
/*     */         
/* 524 */         MsgUtils.msg(s, Lang.CMD_NO_TAGS_LOADED.getConfigValue(null));
/* 525 */         return true;
/*     */       } 
/*     */       
/* 528 */       Player target = Bukkit.getPlayer(args[1]);
/*     */       
/* 530 */       if (target == null) {
/*     */         
/* 532 */         MsgUtils.msg(s, Lang.CMD_TARGET_NOT_ONLINE.getConfigValue(new String[] { args[1] }));
/*     */ 
/*     */         
/* 535 */         return true;
/*     */       } 
/*     */       
/* 538 */       String tag = DeluxeTag.getPlayerTagIdentifier(target);
/*     */       
/* 540 */       if (tag == null || DeluxeTag.getPlayerDisplayTag(target).isEmpty()) {
/*     */         
/* 542 */         MsgUtils.msg(s, Lang.CMD_ADMIN_CLEAR_NO_TAG_SET.getConfigValue(new String[] { target
/* 543 */                 .getName() }));
/*     */         
/* 545 */         return true;
/*     */       } 
/*     */       
/* 548 */       this.plugin.getDummy().setPlayerTag(target);
/* 549 */       this.plugin.removeSavedTag(target.getUniqueId().toString());
/*     */       
/* 551 */       MsgUtils.msg(s, Lang.CMD_ADMIN_CLEAR_SUCCESS.getConfigValue(new String[] { target
/* 552 */               .getName() }));
/*     */ 
/*     */       
/* 555 */       if (target != s)
/*     */       {
/* 557 */         MsgUtils.msg((CommandSender)target, Lang.CMD_ADMIN_CLEAR_SUCCESS_TARGET.getConfigValue(new String[] { s
/* 558 */                 .getName() }));
/*     */       }
/*     */       
/* 561 */       return true;
/*     */     } 
/* 563 */     if (args[0].equalsIgnoreCase("reload")) {
/*     */       
/* 565 */       if (!s.hasPermission("deluxetags.reload")) {
/* 566 */         MsgUtils.msg(s, Lang.CMD_NO_PERMS.getConfigValue(new String[] { "deluxetags.reload" }));
/*     */ 
/*     */         
/* 569 */         return true;
/*     */       } 
/*     */       
/* 572 */       this.plugin.reloadConfig();
/* 573 */       this.plugin.saveConfig();
/* 574 */       DeluxeTag.unloadData();
/* 575 */       int loaded = this.plugin.getCfg().loadTags();
/* 576 */       DeluxeTags.setForceTags(this.plugin.getCfg().forceTags());
/*     */       
/* 578 */       this.plugin.getPlayerFile().reloadConfig();
/* 579 */       this.plugin.getPlayerFile().saveConfig();
/*     */       
/* 581 */       this.plugin.getLangFile().reloadConfig();
/* 582 */       this.plugin.getLangFile().saveConfig();
/* 583 */       this.plugin.loadMessages();
/*     */       
/* 585 */       this.plugin.reloadGUIOptions();
/*     */       
/* 587 */       for (Player online : Bukkit.getServer().getOnlinePlayers()) {
/*     */         
/* 589 */         if (!DeluxeTag.hasTagLoaded(online)) {
/*     */           
/* 591 */           String identifier = this.plugin.getSavedTagIdentifier(online.getUniqueId().toString());
/*     */           
/* 593 */           if (identifier != null && 
/* 594 */             DeluxeTag.getLoadedTag(identifier) != null && 
/* 595 */             DeluxeTag.getLoadedTag(identifier).hasTagPermission(online)) {
/*     */             
/* 597 */             DeluxeTag.getLoadedTag(identifier).setPlayerTag(online);
/*     */             continue;
/*     */           } 
/* 600 */           this.plugin.getDummy().setPlayerTag(online);
/*     */         } 
/*     */       } 
/*     */ 
/*     */       
/* 605 */       MsgUtils.msg(s, Lang.CMD_ADMIN_RELOAD.getConfigValue(new String[] {
/* 606 */               String.valueOf(loaded)
/*     */             }));
/*     */       
/* 609 */       return true;
/*     */     } 
/*     */ 
/*     */     
/* 613 */     MsgUtils.msg(s, Lang.CMD_INCORRECT_USAGE.getConfigValue(null));
/*     */     
/* 615 */     return true;
/*     */   }
/*     */ }


/* Location:              D:\work\DeluxeTags-1.8.1-Release_2.jar!\me\clip\deluxetags\commands\TagCommand.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */