/*    */ package me.clip.deluxetags.config;
/*    */ 
/*    */ import java.io.File;
/*    */ import java.io.IOException;
/*    */ import java.util.logging.Level;
/*    */ import org.bukkit.configuration.file.FileConfiguration;
/*    */ import org.bukkit.configuration.file.YamlConfiguration;
/*    */ import org.bukkit.plugin.java.JavaPlugin;
/*    */ 
/*    */ public class ConfigWrapper
/*    */ {
/*    */   private final JavaPlugin plugin;
/*    */   private FileConfiguration config;
/*    */   private File configFile;
/*    */   private final String folderName;
/*    */   private final String fileName;
/*    */   
/*    */   public ConfigWrapper(JavaPlugin instance, String folderName, String fileName) {
/* 19 */     this.plugin = instance;
/* 20 */     this.folderName = folderName;
/* 21 */     this.fileName = fileName;
/*    */   }
/*    */   
/*    */   public void createNewFile(String message, String header) {
/* 25 */     reloadConfig();
/* 26 */     saveConfig();
/* 27 */     loadConfig(header);
/*    */     
/* 29 */     if (message != null) {
/* 30 */       this.plugin.getLogger().info(message);
/*    */     }
/*    */   }
/*    */   
/*    */   public FileConfiguration getConfig() {
/* 35 */     if (this.config == null) {
/* 36 */       reloadConfig();
/*    */     }
/* 38 */     return this.config;
/*    */   }
/*    */   
/*    */   public void loadConfig(String header) {
/* 42 */     this.config.options().header(header);
/* 43 */     this.config.options().copyDefaults(true);
/* 44 */     saveConfig();
/*    */   }
/*    */   
/*    */   public void reloadConfig() {
/* 48 */     if (this.configFile == null) {
/* 49 */       if (this.folderName != null && !this.folderName.isEmpty()) {
/* 50 */         this.configFile = new File(this.plugin.getDataFolder() + File.separator + this.folderName, this.fileName);
/*    */       } else {
/* 52 */         this.configFile = new File(this.plugin.getDataFolder(), this.fileName);
/*    */       } 
/*    */     }
/* 55 */     this.config = (FileConfiguration)YamlConfiguration.loadConfiguration(this.configFile);
/*    */   }
/*    */   
/*    */   public void saveConfig() {
/* 59 */     if (this.config == null || this.configFile == null) {
/*    */       return;
/*    */     }
/*    */     try {
/* 63 */       getConfig().save(this.configFile);
/* 64 */     } catch (IOException ex) {
/* 65 */       this.plugin.getLogger().log(Level.SEVERE, "Could not save config to " + this.configFile, ex);
/*    */     } 
/*    */   }
/*    */ }
