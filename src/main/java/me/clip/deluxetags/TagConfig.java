/*     */ package me.clip.deluxetags;
/*     */ 
/*     */ import java.util.Arrays;
/*     */ import java.util.Collections;
/*     */ import java.util.Iterator;
/*     */ import java.util.Set;
/*     */ import org.bukkit.configuration.file.FileConfiguration;
/*     */ 
/*     */ 
/*     */ public class TagConfig
/*     */ {
/*     */   DeluxeTags plugin;
/*     */   FileConfiguration config;
/*     */   
/*     */   public TagConfig(DeluxeTags i) {
/*  16 */     this.plugin = i;
/*  17 */     this.config = this.plugin.getConfig();
/*     */   }
/*     */ 
/*     */   
/*     */   public void loadDefConfig() {
/*  22 */     this.config.options().header("DeluxeTags version: " + this.plugin
/*  23 */         .getDescription().getVersion() + " Main Configuration\n\nCreate your tags using the following format:\n\ndeluxetags:\n  VIP: \n    order: 1\n    tag: '&7[&eVIP&7]'\n    description: 'This tag is awarded by getting VIP'\n\nPlaceholders for your DeluxeChat formats config:\n\n%deluxetags_identifier% - display the players active tag identifier\n%deluxetags_tag% - display the players active tag\n%deluxetags_description% - display the players active tag description\n%deluxetags_amount% - display the amount of tags a player has access to\n\nPlaceholders for your essentials/chat handling formats config:\n\n{deluxetags_identifier} - display the players active tag identifier\n{deluxetags_tag} - display the players active tag\n{deluxetags_description} - display the players active tag description\n{deluxetags_amount} - display the amount of tags a player has access to");
/*     */
/*  47 */     this.config.addDefault("force_tags", Boolean.FALSE);
/*  48 */     this.config.addDefault("check_updates", Boolean.TRUE);
/*  49 */     this.config.addDefault("deluxe_chat",  Boolean.TRUE);
/*  50 */     this.config.addDefault("format_chat.enabled", Boolean.valueOf(true));
/*  51 */     this.config.addDefault("format_chat.format", "{deluxetags_tag} <%1$s> %2$s");
/*  52 */     if (this.config.contains("force_tag_on_join")) {
/*  53 */       this.config.set("force_tag_on_join", null);
/*     */     }
/*  55 */     this.config.addDefault("load_tag_on_join", Boolean.valueOf(true));
/*  56 */     this.config.addDefault("gui.name", "&6Available tags&f: &6%deluxetags_amount%");
/*  57 */     this.config.addDefault("gui.tag_select_item.material", "NAME_TAG");
/*  58 */     this.config.addDefault("gui.tag_select_item.data", Integer.valueOf(0));
/*  59 */     this.config.addDefault("gui.tag_select_item.displayname", "&6Tag&f: &6%deluxetags_identifier%");
/*  60 */     this.config.addDefault("gui.tag_select_item.lore", 
/*  61 */         Arrays.asList("%deluxetags_tag%", "%deluxetags_description%"));
/*  62 */     this.config.addDefault("gui.divider_item.material", "STAINED_GLASS_PANE");
/*  63 */     this.config.addDefault("gui.divider_item.data", Integer.valueOf(15));
/*  64 */     this.config.addDefault("gui.divider_item.displayname", "");
/*  65 */     this.config.addDefault("gui.divider_item.lore", Collections.emptyList());
/*  66 */     this.config.addDefault("gui.has_tag_item.material", "SKULL_ITEM");
/*  67 */     this.config.addDefault("gui.has_tag_item.data", Integer.valueOf(3));
/*  68 */     this.config.addDefault("gui.has_tag_item.displayname", "&eCurrent tag&f: &6%deluxetags_identifier%");
/*  69 */     this.config.addDefault("gui.has_tag_item.lore", 
/*  70 */         Arrays.asList("%deluxetags_tag%", "Click to remove your current tag"));
/*  71 */     this.config.addDefault("gui.no_tag_item.material", "SKULL_ITEM");
/*  72 */     this.config.addDefault("gui.no_tag_item.data", Integer.valueOf(3));
/*  73 */     this.config.addDefault("gui.no_tag_item.displayname", "&cYou don't have a tag set!");
/*  74 */     this.config.addDefault("gui.no_tag_item.lore", 
/*  75 */         Collections.singletonList("&7Click a tag above to select one!"));
/*  76 */     this.config.addDefault("gui.exit_item.material", "IRON_DOOR");
/*  77 */     this.config.addDefault("gui.exit_item.data", Integer.valueOf(0));
/*  78 */     this.config.addDefault("gui.exit_item.displayname", "&cClick to exit");
/*  79 */     this.config.addDefault("gui.exit_item.lore", Collections.singletonList("&7Exit the tags menu"));

              if(!this.config.contains("database")){
                  this.config.set("database.hostname","localhost");
                  this.config.set("database.port",3306);
                  this.config.set("database.database","");
                  this.config.set("database.username","");
                  this.config.set("database.password","");
              }
/*     */     
/*  81 */     if (!this.config.contains("deluxetags")) {
/*  82 */       this.config.set("deluxetags.example.order", 1);
/*  83 */       this.config.set("deluxetags.example.tag", "&8[&bDeluxeTags&8]");
/*  84 */       this.config.set("deluxetags.example.description", "&cAwarded for using DeluxeTags!");
/*  85 */       this.config.set("deluxetags.example.permission", "deluxetags.tag.example");
/*     */     } 
/*     */     
/*  88 */     this.config.options().copyDefaults(true);
/*  89 */     this.plugin.saveConfig();
/*  90 */     this.plugin.reloadConfig();
/*     */   }
/*     */   
/*     */   public boolean formatChat() {
/*  94 */     return this.config.getBoolean("format_chat.enabled");
/*     */   }
/*     */   
/*     */   public String chatFormat() {
/*  98 */     String format = this.config.getString("format_chat.format");
/*  99 */     return (format != null) ? format : "{deluxetags_tag} <%1$s> %2$s";
/*     */   }
/*     */   
/*     */   public boolean checkUpdates() {
/* 103 */     return this.config.getBoolean("check_updates");
/*     */   }
/*     */   
/*     */   public boolean deluxeChat() {
/* 107 */     return this.config.getBoolean("deluxe_chat");
/*     */   }
/*     */   
/*     */   public boolean loadTagOnJoin() {
/* 111 */     return this.config.getBoolean("load_tag_on_join");
/*     */   }
/*     */   
/*     */   public boolean forceTags() {
/* 115 */     return this.config.getBoolean("force_tags");
/*     */   }
/*     */ 
/*     */   
/*     */   public int loadTags() {
/* 120 */     FileConfiguration c = this.plugin.getConfig();
/*     */     
/* 122 */     int loaded = 0;
/*     */     
/* 124 */     if (!c.contains("deluxetags")) {
/* 125 */       return loaded;
/*     */     }
/*     */     
/* 128 */     Set<String> keys = c.getConfigurationSection("deluxetags").getKeys(false);
/*     */     
/* 130 */     if (keys != null && !keys.isEmpty()) {
/*     */       
/* 132 */       Iterator<String> it = keys.iterator();
/*     */       
/* 134 */       int count = 1;
/*     */       
/* 136 */       boolean save = false;
/*     */       
/* 138 */       while (it.hasNext()) {
/*     */         
/* 140 */         String desc, identifier = it.next();
/*     */         
/* 142 */         String tag = c.getString("deluxetags." + identifier + ".tag");
/*     */ 
/*     */ 
/*     */         
/* 146 */         if (c.isList("deluxetags." + identifier + ".description")) {
/* 147 */           desc = String.join("\n", c.getStringList("deluxetags." + identifier + ".description"));
/*     */         } else {
/* 149 */           desc = c.getString("deluxetags." + identifier + ".description", "&f");
/*     */         } 
/*     */         
/* 152 */         int priority = count;
/*     */         
/* 154 */         count++;
/*     */         
/* 156 */         if (!c.contains("deluxetags." + identifier + ".order")) {
/* 157 */           c.set("deluxetags." + identifier + ".order", priority);
/* 158 */           save = true;
/*     */         } else {
/* 160 */           priority = c.getInt("deluxetags." + identifier + ".order");
/*     */         } 
/*     */         
/* 163 */         DeluxeTag t = new DeluxeTag(priority, identifier, tag, desc);
/* 164 */         t.setPermission(c.getString("deluxetags." + identifier + ".permission", "deluxetags.tag." + identifier));
/* 165 */         t.load();
/* 166 */         loaded++;
/*     */       } 
/*     */       
/* 169 */       if (save) {
/* 170 */         this.plugin.saveConfig();
/* 171 */         this.plugin.reloadConfig();
/*     */       } 
/*     */     } 
/*     */     
/* 175 */     return loaded;
/*     */   }
/*     */   
/*     */   public void saveTag(DeluxeTag tag) {
/* 179 */     saveTag(tag.getPriority(), tag.getIdentifier(), tag.getDisplayTag(), tag.getDescription(), tag.getPermission());
/*     */   }
/*     */   
/*     */   public void saveTag(int priority, String identifier, String tag, String description, String permission) {
/* 183 */     FileConfiguration c = this.plugin.getConfig();
/* 184 */     c.set("deluxetags." + identifier + ".order", priority);
/* 185 */     c.set("deluxetags." + identifier + ".tag", tag);
/* 186 */     if (description == null) {
/* 187 */       description = "&fDescription for tag " + identifier;
/*     */     }
/* 189 */     c.set("deluxetags." + identifier + ".description", description);
/* 190 */     c.set("deluxetags." + identifier + ".permission", permission);
/* 191 */     this.plugin.saveConfig();
/*     */   }
/*     */   
/*     */   public void removeTag(String identifier) {
/* 195 */     FileConfiguration c = this.plugin.getConfig();
/* 196 */     c.set("deluxetags." + identifier, null);
/* 197 */     this.plugin.saveConfig();
/*     */   }
/*     */ }


/* Location:              D:\work\DeluxeTags-1.8.1-Release_2.jar!\me\clip\deluxetags\TagConfig.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */