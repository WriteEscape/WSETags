/*    */ package me.clip.deluxetags.utils;
/*    */ 
/*    */ import org.bukkit.ChatColor;
/*    */ import org.bukkit.command.CommandSender;
/*    */ 
/*    */ public class MsgUtils {
/*    */   public static String color(String input) {
/*  8 */     return ChatColor.translateAlternateColorCodes('&', input);
/*    */   }
/*    */   
/*    */   public static void msg(CommandSender s, String msg) {
/* 12 */     s.sendMessage(color(msg));
/*    */   }
/*    */ }


/* Location:              D:\work\DeluxeTags-1.8.1-Release_2.jar!\me\clip\deluxetag\\utils\MsgUtils.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */