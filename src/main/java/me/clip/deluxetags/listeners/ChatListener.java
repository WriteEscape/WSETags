/*    */ package me.clip.deluxetags.listeners;
/*    */ 
/*    */ import me.clip.deluxetags.DeluxeTags;
/*    */ import org.bukkit.event.EventHandler;
/*    */ import org.bukkit.event.EventPriority;
/*    */ import org.bukkit.event.Listener;
/*    */ import org.bukkit.event.player.AsyncPlayerChatEvent;
/*    */ 
/*    */ 
/*    */ public class ChatListener
/*    */   implements Listener
/*    */ {
/*    */   DeluxeTags plugin;
/*    */   
/*    */   public ChatListener(DeluxeTags i) {
/* 16 */     this.plugin = i;
/*    */   }
/*    */ 
/*    */   
/*    */   @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
/*    */   public void onChat(AsyncPlayerChatEvent e) {
/* 22 */     String format = e.getFormat();
/*    */     
/* 24 */     format = DeluxeTags.setPlaceholders(e.getPlayer(), format, null);
/*    */     
/* 26 */     e.setFormat(format);
/*    */   }
/*    */ }


/* Location:              D:\work\DeluxeTags-1.8.1-Release_2.jar!\me\clip\deluxetags\listeners\ChatListener.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */