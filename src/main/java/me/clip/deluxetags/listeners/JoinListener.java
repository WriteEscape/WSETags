/*    */ package me.clip.deluxetags.listeners;
/*    */ 
/*    */ import me.clip.deluxetags.DeluxeTag;
/*    */ import me.clip.deluxetags.DeluxeTags;
/*    */ import org.bukkit.event.EventHandler;
/*    */ import org.bukkit.event.EventPriority;
/*    */ import org.bukkit.event.Listener;
/*    */ import org.bukkit.event.player.PlayerJoinEvent;
/*    */ 
/*    */ public class JoinListener
/*    */   implements Listener {
/*    */   DeluxeTags plugin;
/*    */   
/*    */   public JoinListener(DeluxeTags i) {
/* 15 */     this.plugin = i;
/*    */   }
/*    */ 
/*    */   
/*    */   @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
/*    */   public void onChat(PlayerJoinEvent e) {
/* 21 */     if (!e.getPlayer().isOp() && 
/* 22 */       DeluxeTags.forceTags()) {
/*    */       
/* 24 */       DeluxeTag tag = DeluxeTag.getForcedTag(e.getPlayer());
/*    */       
/* 26 */       if (tag != null)
/*    */       {
/* 28 */         tag.setPlayerTag(e.getPlayer());
/*    */       }
/*    */     } 
/*    */ 
/*    */     
/* 33 */     if (!DeluxeTag.hasTagLoaded(e.getPlayer())) {
/*    */       
/* 35 */       String identifier = this.plugin.getSavedTagIdentifier(e.getPlayer().getUniqueId().toString());
/*    */       
/* 37 */       if (identifier != null && 
/* 38 */         DeluxeTag.getLoadedTag(identifier) != null && 
/* 39 */         DeluxeTag.getLoadedTag(identifier).hasTagPermission(e.getPlayer())) {
/*    */         
/* 41 */         DeluxeTag.getLoadedTag(identifier).setPlayerTag(e.getPlayer());
/*    */       }
/*    */       else {
/*    */         
/* 45 */         this.plugin.getDummy().setPlayerTag(e.getPlayer());
/* 46 */         this.plugin.removeSavedTag(e.getPlayer().getUniqueId().toString());
/*    */       } 
/*    */     } 
/*    */   }
/*    */ }


/* Location:              D:\work\DeluxeTags-1.8.1-Release_2.jar!\me\clip\deluxetags\listeners\JoinListener.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */