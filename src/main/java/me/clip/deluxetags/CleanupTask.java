/*    */ package me.clip.deluxetags;
/*    */ 
/*    */ import java.util.ArrayList;
/*    */ import java.util.Iterator;
/*    */ import java.util.List;
/*    */ import java.util.UUID;
/*    */ import org.bukkit.Bukkit;
/*    */ import org.bukkit.entity.Player;
/*    */ import org.bukkit.plugin.Plugin;
/*    */ 
/*    */ public class CleanupTask
/*    */   implements Runnable {
/*    */   DeluxeTags plugin;
/*    */   
/*    */   public CleanupTask(DeluxeTags instance) {
/* 16 */     this.plugin = instance;
/*    */   }
/*    */ 
/*    */ 
/*    */   
/*    */   public void run() {
/* 22 */     if (DeluxeTag.getLoadedPlayers() == null || DeluxeTag.getLoadedPlayers().isEmpty()) {
/*    */       return;
/*    */     }
/*    */     
/* 26 */     Bukkit.getScheduler().runTask((Plugin)this.plugin, () -> {
/*    */           Iterator<String> it = DeluxeTag.getLoadedPlayers().iterator();
/*    */           List<String> remove = new ArrayList<>();
/*    */           while (it.hasNext()) {
/*    */             String uuid = it.next();
/*    */             Player p = Bukkit.getServer().getPlayer(UUID.fromString(uuid));
/*    */             if (p == null)
/*    */               remove.add(uuid); 
/*    */           } 
/*    */           if (remove.isEmpty())
/*    */             return; 
/*    */           for (String id : remove)
/*    */             DeluxeTag.removePlayer(id); 
/*    */         });
/*    */   }
/*    */ }


/* Location:              D:\work\DeluxeTags-1.8.1-Release_2.jar!\me\clip\deluxetags\CleanupTask.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */