/*    */ package me.clip.deluxetags.updater;
/*    */ 
/*    */ import java.io.BufferedReader;
/*    */ import java.io.InputStreamReader;
/*    */ import java.net.URL;
/*    */ import javax.net.ssl.HttpsURLConnection;
/*    */ import me.clip.placeholderapi.util.Msg;
/*    */ import org.bukkit.Bukkit;
/*    */ import org.bukkit.command.CommandSender;
/*    */ import org.bukkit.event.EventHandler;
/*    */ import org.bukkit.event.EventPriority;
/*    */ import org.bukkit.event.Listener;
/*    */ import org.bukkit.event.player.PlayerJoinEvent;
/*    */ import org.bukkit.plugin.Plugin;
/*    */ 
/*    */ public class UpdateChecker
/*    */   implements Listener {
/* 18 */   private final int RESOURCE_ID = 4390;
/*    */   
/*    */   private Plugin plugin;
/*    */   private String spigotVersion;
/*    */   
/*    */   public UpdateChecker(Plugin i) {
/* 24 */     this.plugin = i;
/* 25 */     this.pluginVersion = i.getDescription().getVersion();
/*    */   }
/*    */   private String pluginVersion; private boolean updateAvailable;
/*    */   public boolean hasUpdateAvailable() {
/* 29 */     return this.updateAvailable;
/*    */   }
/*    */   
/*    */   public String getSpigotVersion() {
/* 33 */     return this.spigotVersion;
/*    */   }
/*    */   
/*    */   public void fetch() {
/* 37 */
/*    */   }
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   
/*    */   private boolean spigotIsNewer() {
/* 69 */     if (this.spigotVersion == null || this.spigotVersion.isEmpty()) {
/* 70 */       return false;
/*    */     }
/* 72 */     String plV = toReadable(this.pluginVersion);
/* 73 */     String spV = toReadable(this.spigotVersion);
/* 74 */     return (plV.compareTo(spV) < 0);
/*    */   }
/*    */   
/*    */   private String toReadable(String version) {
/* 78 */     if (version.contains("-DEV-")) {
/* 79 */       version = version.split("-DEV-")[0];
/*    */     }
/* 81 */     return version.replaceAll("\\.", "");
/*    */   }
/*    */   
/*    */   @EventHandler(priority = EventPriority.MONITOR)
/*    */   public void onJoin(PlayerJoinEvent e) {
/* 86 */     if (e.getPlayer().hasPermission("deluxetags.updates"))
/* 87 */       Msg.msg((CommandSender)e.getPlayer(), new String[] { "&bAn update for &5&lDeluxeTags &e(&5&lDeluxeTags &fv" + 
/* 88 */             getSpigotVersion() + "&e)", "&bis available at &ehttps://www.spigotmc.org/resources/deluxetags.4390/" }); 
/*    */   }
/*    */ }


/* Location:              D:\work\DeluxeTags-1.8.1-Release_2.jar!\me\clip\deluxetag\\updater\UpdateChecker.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */