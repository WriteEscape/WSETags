/*     */ package me.clip.deluxetags;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.Collection;
/*     */ import java.util.HashMap;
/*     */ import java.util.Iterator;
/*     */ import java.util.List;
/*     */ import java.util.Map;
/*     */ import java.util.Set;
/*     */ import java.util.TreeMap;
/*     */ import java.util.stream.Collectors;
/*     */ import me.clip.placeholderapi.PlaceholderAPI;
/*     */ import org.bukkit.entity.Player;

/*     */ public class DeluxeTag
/*     */ {
/*     */   private static TreeMap<Integer, DeluxeTag> configTags;
/*     */   private static Map<String, DeluxeTag> playerTags;
/*     */   private String identifier;
/*     */   private String displayTag;
/*     */   private String description;
/*     */   private String permission;
/*     */   private int priority;
/*     */   
/*     */   public DeluxeTag(int priority, String identifier, String displayTag, String description) {
/*  36 */     this.priority = priority;
/*  37 */     this.identifier = identifier;
/*  38 */     this.displayTag = displayTag;
/*  39 */     this.description = description;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public String getIdentifier() {
/*  47 */     return this.identifier;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public String getDisplayTag() {
/*  55 */     return this.displayTag;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public String getDescription() {
/*  63 */     return this.description;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setDisplayTag(String newDisplayTag) {
/*  71 */     this.displayTag = newDisplayTag;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setDescription(String newDescription) {
/*  79 */     this.description = newDescription;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public int getPriority() {
/*  87 */     return this.priority;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setPriority(int priority) {
/*  95 */     this.priority = priority;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public String getPermission() {
/* 103 */     return (this.permission == null) ? ("deluxetags.tag." + this.identifier) : this.permission;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setPermission(String permission) {
/* 111 */     this.permission = permission;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void load() {
/* 118 */     if (configTags == null) {
/* 119 */       configTags = new TreeMap<>();
/*     */     }
/*     */     
/* 122 */     configTags.put(this.priority, this);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public boolean unload() {
/* 130 */     if (configTags == null || configTags.isEmpty()) {
/* 131 */       return false;
/*     */     }
/*     */     
/* 134 */     if (configTags.containsKey(this.priority)) {
/* 135 */       configTags.remove(this.priority);
/* 136 */       return true;
/*     */     } 
/*     */     
/* 139 */     return false;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public boolean hasTagPermission(Player p) {
/* 148 */     return p.hasPermission(getPermission());
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public boolean hasForceTagPermission(Player p) {
/* 157 */     return p.hasPermission("deluxetags.forcetag." + this.identifier);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public boolean setPlayerTag(Player p) {
/* 166 */     return setPlayerTag(p.getUniqueId().toString());
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public boolean setPlayerTag(String uuid) {
/* 175 */     if (playerTags == null) {
/* 176 */       playerTags = new HashMap<>();
/*     */     }
/*     */     
/* 179 */     if (playerTags.isEmpty()) {
/* 180 */       playerTags.put(uuid, this);
/* 181 */       return true;
/*     */     } 
/*     */     
/* 184 */     if (playerTags.containsKey(uuid) && playerTags.get(uuid) == this) {
/* 185 */       return false;
/*     */     }
/*     */     
/* 188 */     playerTags.put(uuid, this);
/* 189 */     return true;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public List<String> removeActivePlayers() {
/* 198 */     if (playerTags == null || playerTags.isEmpty()) {
/* 199 */       return null;
/*     */     }
/*     */     
/* 202 */     List<String> remove = new ArrayList<>();
/*     */     
/* 204 */     for (String uuid : playerTags.keySet()) {
/* 205 */       if (getPlayerDisplayTag(uuid).equals(this.displayTag)) {
/* 206 */         remove.add(uuid);
/*     */       }
/*     */     } 
/* 209 */     if (!remove.isEmpty()) {
/* 210 */       for (String uuid : remove) {
/* 211 */         removePlayer(uuid);
/*     */       }
/*     */     }
/*     */     
/* 215 */     return remove;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public static Collection<DeluxeTag> getLoadedTags() {
/* 223 */     if (configTags == null) {
/* 224 */       return null;
/*     */     }
/* 226 */     return configTags.values();
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public static boolean hasTagLoaded(String uuid) {
/* 235 */     if (playerTags == null || playerTags.isEmpty()) {
/* 236 */       return false;
/*     */     }
/* 238 */     return playerTags.containsKey(uuid);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public static boolean hasTagLoaded(Player player) {
/* 247 */     return hasTagLoaded(player.getUniqueId().toString());
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public static String getPlayerTagIdentifier(Player p) {
/* 256 */     return getPlayerTagIdentifier(p.getUniqueId().toString());
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public static DeluxeTag getTag(String uuid) {
/* 266 */     if (playerTags == null) {
/* 267 */       playerTags = new HashMap<>();
/*     */     }
/*     */     
/* 270 */     if (playerTags.isEmpty()) {
/* 271 */       return null;
/*     */     }
/*     */     
/* 274 */     if (playerTags.containsKey(uuid) && playerTags.get(uuid) != null) {
/* 275 */       return playerTags.get(uuid);
/*     */     }
/*     */     
/* 278 */     return null;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public static String getPlayerTagIdentifier(String uuid) {
/* 288 */     if (playerTags == null) {
/* 289 */       playerTags = new HashMap<>();
/*     */     }
/*     */     
/* 292 */     if (playerTags.isEmpty()) {
/* 293 */       return null;
/*     */     }
/*     */     
/* 296 */     /* 297 */
              if (playerTags.containsKey(uuid) && playerTags.get(uuid) != null && playerTags.get(uuid).getIdentifier() != null) {
/* 298 */       return playerTags.get(uuid).getIdentifier();
/*     */     }
/*     */ 
/*     */     
/* 302 */     return null;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public static String getPlayerDisplayTag(Player player) {
/* 312 */     String d = getPlayerDisplayTag(player.getUniqueId().toString());
/* 313 */     return DeluxeTags.papi() ? PlaceholderAPI.setPlaceholders(player, d) : d;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public static String getPlayerDisplayTag(String uuid) {
/* 323 */     if (playerTags == null) {
/* 324 */       playerTags = new HashMap<>();
/*     */     }
/*     */     
/* 327 */     if (playerTags.isEmpty()) {
/* 328 */       return "";
/*     */     }
/*     */     
/* 331 */     if (playerTags.containsKey(uuid) && playerTags.get(uuid) != null && (
/* 332 */       (DeluxeTag)playerTags.get(uuid)).getDisplayTag() != null) {
/* 333 */       return ((DeluxeTag)playerTags.get(uuid)).getDisplayTag();
/*     */     }
/*     */ 
/*     */     
/* 337 */     return "";
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public static String getPlayerTagDescription(Player p) {
/* 347 */     String d = getPlayerTagDescription(p.getUniqueId().toString());
/* 348 */     return DeluxeTags.papi() ? PlaceholderAPI.setPlaceholders(p, d) : d;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public static String getPlayerTagDescription(String uuid) {
/* 358 */     if (playerTags == null) {
/* 359 */       playerTags = new HashMap<>();
/*     */     }
/*     */     
/* 362 */     if (playerTags.isEmpty()) {
/* 363 */       return "";
/*     */     }
/*     */     
/* 366 */
/* 367 */     if (playerTags.containsKey(uuid) && playerTags.get(uuid) != null && playerTags.get(uuid).getDescription() != null) {
/* 368 */       return playerTags.get(uuid).getDescription();
/*     */     }
/*     */ 
/*     */     
/* 372 */     return "";
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public static DeluxeTag getLoadedTag(String identifier) {
/* 381 */     if (configTags != null && !configTags.isEmpty())
/*     */     {
/* 383 */       for (DeluxeTag t : getLoadedTags()) {
/* 384 */         if (t.getIdentifier().equals(identifier)) {
/* 385 */           return t;
/*     */         }
/*     */       } 
/*     */     }
/*     */     
/* 390 */     return null;
/*     */   }
/*     */ 
/*     */   
/*     */   public static DeluxeTag getForcedTag(Player p) {
/* 395 */     if (getLoadedTags() == null || getLoadedTags().isEmpty()) {
/* 396 */       return null;
/*     */     }
/* 398 */     Iterator<Integer> it = configTags.keySet().iterator();
/*     */ 
/*     */ 
/*     */     
/* 402 */     while (it.hasNext()) {
/*     */       
/* 404 */       DeluxeTag t = configTags.get(it.next());
/*     */       
/* 406 */       if (t != null && t.hasForceTagPermission(p)) {
/* 407 */         return t;
/*     */       }
/*     */     } 
/* 410 */     return null;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public static List<String> getAvailableTagIdentifiers(Player p) {
/* 420 */     if (getLoadedTags() == null || getLoadedTags().isEmpty()) {
/* 421 */       return null;
/*     */     }
/*     */     
/* 424 */     return getLoadedTags().stream().filter(t -> t.hasTagPermission(p)).map(DeluxeTag::getIdentifier).collect(Collectors.toList());
/*     */   }
/*     */ 
/*     */   
/*     */   public static List<String> getAllTagIdentifiers() {
/* 429 */     if (getLoadedTags() == null || getLoadedTags().isEmpty()) {
/* 430 */       return null;
/*     */     }
/*     */     
/* 433 */     return getLoadedTags().stream().map(DeluxeTag::getIdentifier).collect(Collectors.toList());
/*     */   }
/*     */   
/*     */   public static int getLoadedTagsAmount() {
/* 437 */     if (configTags == null || configTags.isEmpty()) {
/* 438 */       return 0;
/*     */     }
/* 440 */     return configTags.size();
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public static Set<String> getLoadedPlayers() {
/* 448 */     if (playerTags == null || playerTags.isEmpty()) {
/* 449 */       return null;
/*     */     }
/*     */     
/* 452 */     return playerTags.keySet();
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public static void removePlayer(String uuid) {
/* 460 */     if (hasTagLoaded(uuid)) {
/* 461 */       playerTags.remove(uuid);
/*     */     }
/*     */   }
/*     */   
/*     */   public static void unloadData() {
/* 466 */     configTags = null;
/* 467 */     playerTags = null;
/*     */   }
/*     */ }


/* Location:              D:\work\DeluxeTags-1.8.1-Release_2.jar!\me\clip\deluxetags\DeluxeTag.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */