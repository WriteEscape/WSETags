/*     */ package me.clip.deluxetags.gui;
/*     */ 
/*     */ import java.util.Arrays;
/*     */ import java.util.List;
/*     */ import me.clip.deluxetags.DeluxeTags;
/*     */ import org.bukkit.Material;
/*     */ import org.bukkit.configuration.file.FileConfiguration;
/*     */ 
/*     */ 
/*     */ 
/*     */ public class GUIOptions
/*     */ {
/*  13 */   final List<String> ITEM_TYPES = Arrays.asList(new String[] { "tag_select_item", "divider_item", "has_tag_item", "no_tag_item", "exit_item" });
/*     */   
/*     */   private String menuName;
/*     */   
/*     */   private DisplayItem tagSelectItem;
/*     */   
/*     */   private DisplayItem dividerItem;
/*     */   
/*     */   private DisplayItem hasTagItem;
/*     */   
/*     */   private DisplayItem noTagItem;
/*     */   
/*     */   private DisplayItem exitItem;
/*     */ 
/*     */   
/*     */   public GUIOptions(DeluxeTags i) {
/*  29 */     FileConfiguration c = i.getConfig();
/*     */     
/*  31 */     this.menuName = c.getString("gui.name");
/*     */     
/*  33 */     if (this.menuName == null) {
/*  34 */       this.menuName = "&6Available tags&f: &6%deluxetags_amount%";
/*     */     }
/*     */     
/*  37 */     Material mat = null;
/*  38 */     short data = 0;
/*  39 */     String display = null;
/*  40 */     List<String> lore = null;
/*     */     
/*  42 */     for (String type : this.ITEM_TYPES) {
/*     */       
/*     */       try {
/*  45 */         mat = Material.getMaterial(c.getString("gui." + type + ".material")
/*  46 */             .toUpperCase());
/*  47 */       } catch (Exception e) {
/*  48 */         switch (type) {
/*     */           case "tag_select_item":
/*  50 */             mat = Material.NAME_TAG;
/*     */             break;
/*     */           case "divider_item":
/*  53 */             mat = Material.GRAY_STAINED_GLASS_PANE;
/*     */             break;
/*     */           case "has_tag_item":
/*     */           case "no_tag_item":
/*  57 */             mat = Material.PLAYER_HEAD;
/*     */             break;
/*     */           case "exit_item":
/*  60 */             mat = Material.IRON_DOOR;
/*     */             break;
/*     */         } 
/*     */       
/*     */       } 
/*     */       try {
/*  66 */         data = Short.parseShort(c.getString("gui." + type + ".data"));
/*  67 */       } catch (Exception e) {
/*  68 */         data = 0;
/*     */       } 
/*     */       
/*  71 */       display = c.getString("gui." + type + ".displayname");
/*     */       
/*  73 */       lore = c.getStringList("gui." + type + ".lore");
/*     */       
/*  75 */       switch (type) {
/*     */         case "tag_select_item":
/*  77 */           this.tagSelectItem = new DisplayItem(mat, data, display, lore);
/*     */           break;
/*     */         case "divider_item":
/*  80 */           this.dividerItem = new DisplayItem(mat, data, display, lore);
/*     */           break;
/*     */         case "has_tag_item":
/*  83 */           this.hasTagItem = new DisplayItem(mat, data, display, lore);
/*     */           break;
/*     */         case "no_tag_item":
/*  86 */           this.noTagItem = new DisplayItem(mat, data, display, lore);
/*     */           break;
/*     */         case "exit_item":
/*  89 */           this.exitItem = new DisplayItem(mat, data, display, lore);
/*     */           break;
/*     */       } 
/*     */       
/*  93 */       mat = null;
/*  94 */       data = 0;
/*  95 */       display = null;
/*  96 */       lore = null;
/*     */     } 
/*     */   }
/*     */ 
/*     */   
/*     */   public DisplayItem getTagSelectItem() {
/* 102 */     return this.tagSelectItem;
/*     */   }
/*     */   
/*     */   public DisplayItem getDividerItem() {
/* 106 */     return this.dividerItem;
/*     */   }
/*     */   
/*     */   public DisplayItem getHasTagItem() {
/* 110 */     return this.hasTagItem;
/*     */   }
/*     */   
/*     */   public DisplayItem getNoTagItem() {
/* 114 */     return this.noTagItem;
/*     */   }
/*     */   
/*     */   public DisplayItem getExitItem() {
/* 118 */     return this.exitItem;
/*     */   }
/*     */   
/*     */   public String getMenuName() {
/* 122 */     return this.menuName;
/*     */   }
/*     */ }


/* Location:              D:\work\DeluxeTags-1.8.1-Release_2.jar!\me\clip\deluxetags\gui\GUIOptions.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */