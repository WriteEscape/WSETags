/*    */ package me.clip.deluxetags.gui;
/*    */ 
/*    */ import java.util.List;
/*    */ import org.bukkit.Material;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class DisplayItem
/*    */ {
/*    */   private Material material;
/*    */   private short data;
/*    */   private String name;
/*    */   private List<String> lore;
/*    */   
/*    */   public DisplayItem(Material material, short data, String name, List<String> lore) {
/* 18 */     setMaterial(material);
/* 19 */     setData(data);
/* 20 */     setName(name);
/* 21 */     setLore(lore);
/*    */   }
/*    */   
/*    */   public Material getMaterial() {
/* 25 */     return this.material;
/*    */   }
/*    */   
/*    */   public void setMaterial(Material material) {
/* 29 */     this.material = material;
/*    */   }
/*    */   
/*    */   public short getData() {
/* 33 */     return this.data;
/*    */   }
/*    */   
/*    */   public void setData(short data) {
/* 37 */     this.data = data;
/*    */   }
/*    */   
/*    */   public String getName() {
/* 41 */     return this.name;
/*    */   }
/*    */   
/*    */   public void setName(String name) {
/* 45 */     this.name = name;
/*    */   }
/*    */   
/*    */   public List<String> getLore() {
/* 49 */     return this.lore;
/*    */   }
/*    */   
/*    */   public void setLore(List<String> lore) {
/* 53 */     this.lore = lore;
/*    */   }
/*    */ }
