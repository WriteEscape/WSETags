/*     */ package me.clip.deluxetags.gui;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.HashMap;
/*     */ import java.util.List;
/*     */ import java.util.Map;
/*     */ import me.clip.deluxetags.utils.MsgUtils;
/*     */ import org.bukkit.Bukkit;
/*     */ import org.bukkit.Material;
/*     */ import org.bukkit.entity.Player;
/*     */ import org.bukkit.inventory.Inventory;
/*     */ import org.bukkit.inventory.ItemStack;
/*     */ import org.bukkit.inventory.meta.ItemMeta;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class TagGUI
/*     */ {
/*     */   private static HashMap<String, TagGUI> inGUI;
/*     */   private Inventory inventory;
/*     */   private String displayName;
/*     */   private int slots;
/*     */   private int page;
/*     */   private Map<Integer, String> tags;
/*     */   private Map<Integer, ItemStack> items;
/*     */   
/*     */   public TagGUI(String displayName, int page) {
/*  33 */     this.displayName = displayName;
/*  34 */     this.items = new HashMap<>();
/*  35 */     this.page = page;
/*  36 */     this.tags = new HashMap<>();
/*     */   }
/*     */   
/*     */   public int getInventorySize() {
/*  40 */     return this.slots;
/*     */   }
/*     */   
/*     */   public String getInventoryName() {
/*  44 */     return this.displayName;
/*     */   }
/*     */   
/*     */   public TagGUI clear() {
/*  48 */     this.items.clear();
/*  49 */     this.tags = null;
/*  50 */     return this;
/*     */   }
/*     */   
/*     */   public boolean contains(ItemStack item) {
/*  54 */     return this.items.containsValue(item);
/*     */   }
/*     */   
/*     */   public TagGUI setItem(int slot, ItemStack item) {
/*  58 */     this.items.put(Integer.valueOf(slot), item);
/*  59 */     return this;
/*     */   }
/*     */   
/*     */   public TagGUI setSlots(int slots) {
/*  63 */     this.slots = slots;
/*  64 */     return this;
/*     */   }
/*     */ 
/*     */   
/*     */   public void openInventory(Player player) {
/*  69 */     this.inventory = Bukkit.createInventory(null, this.slots, MsgUtils.color(this.displayName));
/*     */     
/*  71 */     for (Integer slot : this.items.keySet()) {
/*  72 */       this.inventory.setItem(slot.intValue(), this.items.get(slot));
/*     */     }
/*  74 */     player.openInventory(this.inventory);
/*     */     
/*  76 */     if (inGUI == null) {
/*  77 */       inGUI = new HashMap<>();
/*     */     }
/*     */     
/*  80 */     inGUI.put(player.getName(), this);
/*     */   }
/*     */   
/*     */   public static ItemStack createItem(Material mat, short data, int amount, String displayName, List<String> lore) {
/*  84 */     if (mat == null) {
/*  85 */       return null;
/*     */     }
/*  87 */     ItemStack i = new ItemStack(mat, amount);
/*  88 */
/*  91 */     ItemMeta im = i.getItemMeta();
/*  92 */     if (displayName != null) {
/*  93 */       im.setDisplayName(MsgUtils.color(displayName));
/*     */     }
/*  95 */     if (lore != null && !lore.isEmpty()) {
/*  96 */       List<String> temp = new ArrayList<>();
/*  97 */       for (String line : lore) {
/*  98 */         temp.add(MsgUtils.color(line));
/*     */       }
/* 100 */       im.setLore(temp);
/*     */     } 
/* 102 */     i.setItemMeta(im);
/* 103 */     return i;
/*     */   }
/*     */ 
/*     */ 
/*     */   
/*     */   public static boolean hasGUI(Player p) {
/* 109 */     if (inGUI == null) {
/* 110 */       return false;
/*     */     }
/*     */     
/* 113 */     return (inGUI.containsKey(p.getName()) && inGUI.get(p.getName()) != null);
/*     */   }
/*     */ 
/*     */   
/*     */   public static TagGUI getGUI(Player p) {
/* 118 */     if (!hasGUI(p)) {
/* 119 */       return null;
/*     */     }
/* 121 */     return inGUI.get(p.getName());
/*     */   }
/*     */   
/*     */   public static boolean close(Player p) {
/* 125 */     if (!hasGUI(p)) {
/* 126 */       return false;
/*     */     }
/*     */     
/* 129 */     getGUI(p).clear();
/* 130 */     inGUI.remove(p.getName());
/* 131 */     return true;
/*     */   }
/*     */   
/*     */   public int getPage() {
/* 135 */     return this.page;
/*     */   }
/*     */   
/*     */   public void setPage(int page) {
/* 139 */     this.page = page;
/*     */   }
/*     */   
/*     */   public static void unload() {
/* 143 */     inGUI = null;
/*     */   }
/*     */   
/*     */   public Map<Integer, String> getTags() {
/* 147 */     return this.tags;
/*     */   }
/*     */   
/*     */   public void setTags(Map<Integer, String> tags) {
/* 151 */     this.tags = tags;
/*     */   }
/*     */ }


/* Location:              D:\work\DeluxeTags-1.8.1-Release_2.jar!\me\clip\deluxetags\gui\TagGUI.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */