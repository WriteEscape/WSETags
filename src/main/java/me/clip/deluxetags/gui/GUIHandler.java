/*     */ package me.clip.deluxetags.gui;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.Arrays;
/*     */ import java.util.HashMap;
/*     */ import java.util.List;
/*     */ import java.util.Map;
/*     */ import me.clip.deluxetags.DeluxeTag;
/*     */ import me.clip.deluxetags.DeluxeTags;
/*     */ import me.clip.deluxetags.Lang;
/*     */ import me.clip.deluxetags.utils.MsgUtils;
/*     */ import org.bukkit.Material;
/*     */ import org.bukkit.entity.Player;
/*     */ import org.bukkit.event.EventHandler;
/*     */ import org.bukkit.event.Listener;
/*     */ import org.bukkit.event.inventory.InventoryClickEvent;
/*     */ import org.bukkit.event.inventory.InventoryCloseEvent;
/*     */ import org.bukkit.inventory.ItemStack;
/*     */ import org.bukkit.inventory.meta.ItemMeta;
/*     */ 
/*     */ public class GUIHandler
/*     */   implements Listener
/*     */ {
/*     */   private DeluxeTags plugin;
/*     */   
/*     */   public GUIHandler(DeluxeTags i) {
/*  27 */     this.plugin = i;
/*     */   }
/*     */   
/*     */   private void sms(Player p, String msg) {
/*  31 */     p.sendMessage(MsgUtils.color(msg));
/*     */   }
/*     */ 
/*     */   
/*     */   @EventHandler
/*     */   public void onInventoryClick(InventoryClickEvent e) {
/*  37 */     Player p = (Player)e.getWhoClicked();
/*     */     
/*  39 */     if (!TagGUI.hasGUI(p)) {
/*     */       return;
/*     */     }
/*     */     
/*  43 */     TagGUI gui = TagGUI.getGUI(p);
/*     */     
/*  45 */     e.setCancelled(true);
/*     */     
/*  47 */     ItemStack clicked = e.getCurrentItem();
/*     */     
/*  49 */     if (clicked == null || clicked.getType().equals(Material.AIR)) {
/*     */       return;
/*     */     }
/*     */     
/*  53 */     int slot = e.getRawSlot();
/*     */     
/*  55 */     if (slot < 36) {
/*     */       
/*  57 */       if (clicked.getType() != null && !clicked.getType().equals(Material.AIR)) {
/*     */         Map<Integer, String> tags;
/*     */         
/*     */         try {
/*  61 */           tags = gui.getTags();
/*  62 */         } catch (NullPointerException ex) {
/*  63 */           TagGUI.close(p);
/*  64 */           p.closeInventory();
/*     */           
/*     */           return;
/*     */         } 
/*     */         
/*  69 */         if (tags.isEmpty()) {
/*  70 */           TagGUI.close(p);
/*     */           
/*  72 */           p.closeInventory();
/*     */           
/*     */           return;
/*     */         } 
/*  76 */         String id = tags.get(Integer.valueOf(slot));
/*     */         
/*  78 */         if (id == null || id.isEmpty()) {
/*  79 */           TagGUI.close(p);
/*     */           
/*  81 */           p.closeInventory();
/*     */ 
/*     */           
/*     */           return;
/*     */         } 
/*     */         
/*  87 */         if (DeluxeTag.getLoadedTag(id) != null && DeluxeTag.getLoadedTag(id).setPlayerTag(p)) {
/*  88 */           TagGUI.close(p);
/*  89 */           p.closeInventory();
/*     */           
/*  91 */           sms(p, Lang.GUI_TAG_SELECTED.getConfigValue(new String[] { id, 
/*  92 */                   DeluxeTag.getPlayerDisplayTag(p) }));
/*     */ 
/*     */           
/*  95 */           this.plugin.saveTagIdentifier(p.getUniqueId().toString(), id);
/*     */         }
/*     */       
/*     */       } 
/*  99 */     } else if (slot == 48 || slot == 50) {
/*     */       
/* 101 */       TagGUI.close(p);
/* 102 */       p.closeInventory();
/*     */     }
/* 104 */     else if (slot == 49) {
/*     */       
/* 106 */       if (!DeluxeTag.getPlayerDisplayTag(p).isEmpty()) {
/*     */         
/* 108 */         TagGUI.close(p);
/*     */         
/* 110 */         p.closeInventory();
/*     */         
/* 112 */         this.plugin.getDummy().setPlayerTag(p);
/*     */         
/* 114 */         this.plugin.removeSavedTag(p.getUniqueId().toString());
/*     */         
/* 116 */         sms(p, Lang.GUI_TAG_DISABLED.getConfigValue(null));
/*     */       } 
/*     */       
/* 119 */       p.updateInventory();
/*     */     }
/* 121 */     else if (slot == 45 || slot == 53) {
/*     */       
/* 123 */       if (clicked.hasItemMeta() && clicked.getItemMeta().hasDisplayName()) {
/*     */         String name;
/*     */         
/*     */         int page;
/* 127 */         if (slot == 45) {
/* 128 */           name = clicked.getItemMeta().getDisplayName().replace("Back to page ", "");
/*     */         } else {
/* 130 */           name = clicked.getItemMeta().getDisplayName().replace("Forward to page ", "");
/*     */         } 
/*     */ 
/*     */ 
/*     */ 
/*     */         
/*     */         try {
/* 137 */           page = Integer.parseInt(name);
/*     */         }
/* 139 */         catch (Exception ex) {
/*     */           
/* 141 */           TagGUI.close(p);
/*     */           
/* 143 */           p.closeInventory();
/*     */           
/* 145 */           sms(p, Lang.GUI_PAGE_ERROR.getConfigValue(null));
/*     */           
/*     */           return;
/*     */         } 
/* 149 */         openMenu(p, page);
/*     */       } 
/*     */     } 
/*     */   }
/*     */ 
/*     */   
/*     */   @EventHandler
/*     */   public void onClose(InventoryCloseEvent e) {
/* 157 */     if (e.getPlayer() instanceof Player) {
/*     */       
/* 159 */       Player p = (Player)e.getPlayer();
/*     */       
/* 161 */       if (TagGUI.hasGUI(p))
/*     */       {
/* 163 */         TagGUI.close(p);
/*     */       }
/*     */     } 
/*     */   }
/*     */ 
/*     */   
/*     */   public boolean openMenu(Player p, int page) {
/*     */     DisplayItem item;
/* 171 */     List<String> ids = DeluxeTag.getAvailableTagIdentifiers(p);
/*     */     
/* 173 */     if (ids == null || ids.isEmpty()) {
/* 174 */       return false;
/*     */     }
/*     */     
/* 177 */     GUIOptions options = this.plugin.getGuiOptions();
/*     */     
/* 179 */     String title = options.getMenuName();
/*     */     
/* 181 */     title = DeluxeTags.setPlaceholders(p, title, null);
/*     */     
/* 183 */     if (title.length() > 32) {
/* 184 */       title = title.substring(0, 31);
/*     */     }
/*     */     
/* 187 */     TagGUI gui = (new TagGUI(title, page)).setSlots(54);
/*     */     
/* 189 */     int pages = 1;
/*     */     
/* 191 */     if (ids.size() > 36) {
/*     */       
/* 193 */       pages = ids.size() / 36;
/*     */       
/* 195 */       if (ids.size() % 36 > 0) {
/* 196 */         pages++;
/*     */       }
/*     */     } 
/*     */     
/* 200 */     if (page > 1 && page <= pages) {
/*     */       
/* 202 */       int start = 36 * page - 36;
/*     */       
/* 204 */       ids = ids.subList(start, ids.size());
/*     */     } 
/*     */     
/* 207 */     int count = 0;
/*     */     
/* 209 */     Map<Integer, String> tags = new HashMap<>();
/*     */     
/* 211 */     for (String id : ids) {
/*     */       
/* 213 */       if (count >= 36) {
/*     */         break;
/*     */       }
/*     */       
/* 217 */       tags.put(count, id);
/*     */       
/* 219 */       DeluxeTag tag = DeluxeTag.getLoadedTag(id);
/*     */       
/* 221 */       if (tag == null) {
/* 222 */         tag = new DeluxeTag(1, "", "", "");
/*     */       }
/*     */       
/* 225 */       String str1 = options.getTagSelectItem().getName();
/*     */       
/* 227 */       str1 = DeluxeTags.setPlaceholders(p, str1, tag);
/*     */       
/* 229 */       List<String> list1 = null;
/*     */       
/* 231 */       List<String> list2 = options.getTagSelectItem().getLore();
/* 232 */       if (list2 != null && !list2.isEmpty()) {
/* 233 */         list1 = new ArrayList<>();
/* 234 */         for (String line : list2) {
/* 235 */           line = DeluxeTags.setPlaceholders(p, line, tag);
/* 236 */           if (line.contains("\n")) {
/* 237 */             list1.addAll(Arrays.asList(line.split("\n"))); continue;
/*     */           } 
/* 239 */           list1.add(line);
/*     */         } 
/*     */       } 
/*     */       
/* 243 */       gui.setItem(count, TagGUI.createItem(options.getTagSelectItem().getMaterial(), options.getTagSelectItem().getData(), 1, str1, list1));
/*     */       
/* 245 */       count++;
/*     */     } 
/*     */     
/* 248 */     gui.setTags(tags);
/*     */ 
/*     */ 
/*     */     
/* 252 */     String display = options.getDividerItem().getName();
/* 253 */     display = MsgUtils.color(DeluxeTags.setPlaceholders(p, display, null));
/* 254 */     List<String> tmp = null;
/*     */     
/* 256 */     List<String> orig = options.getDividerItem().getLore();
/* 257 */     if (orig != null && !orig.isEmpty()) {
/* 258 */       tmp = new ArrayList<>();
/* 259 */       for (String line : orig) {
/* 260 */         line = DeluxeTags.setPlaceholders(p, line, null);
/* 261 */         if (line.contains("\n")) {
/* 262 */           tmp.addAll(Arrays.asList(line.split("\n")));
/*     */         } else {
/* 264 */           tmp.add(line);
/*     */         } 
/* 266 */         tmp.add(line);
/*     */       } 
/*     */     } 
/*     */     
/* 270 */     ItemStack divider = TagGUI.createItem(options.getDividerItem().getMaterial(), options.getDividerItem().getData(), 1, display, tmp);
/*     */     
/* 272 */     for (int b = 36; b < 45; b++)
/*     */     {
/* 274 */       gui.setItem(b, divider);
/*     */     }
/*     */ 
/*     */ 
/*     */     
/* 279 */     String current = DeluxeTag.getPlayerTagIdentifier(p);
/*     */ 
/*     */ 
/*     */     
/* 283 */     if (current == null || current.isEmpty()) {
/* 284 */       item = options.getNoTagItem();
/*     */     } else {
/* 286 */       item = options.getHasTagItem();
/*     */     } 
/*     */     
/* 289 */     ItemStack info = new ItemStack(item.getMaterial(), 1);
/*     */     
/* 291 */     ItemMeta meta = info.getItemMeta();
/*     */     
/* 293 */     meta.setDisplayName(DeluxeTags.setPlaceholders(p, item.getName(), null));
/*     */     
/* 295 */     if (item.getLore() != null) {
/*     */ 
/*     */ 
/*     */       
/* 299 */       List<String> infoLore = item.getLore();
/*     */       
/* 301 */       if (infoLore != null && !infoLore.isEmpty()) {
/* 302 */         List<String> infoTmp = new ArrayList<>();
/*     */         
/* 304 */         for (String line : infoLore) {
/* 305 */           line = DeluxeTags.setPlaceholders(p, line, null);
/* 306 */           if (line.contains("\n")) {
/* 307 */             infoTmp.addAll(Arrays.asList(line.split("\n"))); continue;
/*     */           } 
/* 309 */           infoTmp.add(line);
/*     */         } 
/*     */ 
/*     */         
/* 313 */         meta.setLore(infoTmp);
/*     */       } 
/*     */     } 
/*     */     
/* 317 */     info.setItemMeta(meta);
/*     */     
/* 319 */     gui.setItem(49, info);
/*     */     
/* 321 */     String exitDisplay = DeluxeTags.setPlaceholders(p, options.getExitItem().getName(), null);
/*     */     
/* 323 */     List<String> exitLore = options.getExitItem().getLore();
/*     */     
/* 325 */     List<String> exitTmp = null;
/*     */     
/* 327 */     if (exitLore != null && !exitLore.isEmpty()) {
/*     */       
/* 329 */       exitTmp = new ArrayList<>();
/*     */       
/* 331 */       for (String line : exitLore) {
/* 332 */         line = DeluxeTags.setPlaceholders(p, line, null);
/* 333 */         exitTmp.add(line);
/*     */       } 
/*     */     } 
/*     */     
/* 337 */     ItemStack exit = TagGUI.createItem(options.getExitItem().getMaterial(), options.getExitItem().getData(), 1, exitDisplay, exitTmp);
/*     */     
/* 339 */     gui.setItem(48, exit);
/* 340 */     gui.setItem(50, exit);
/*     */ 
/*     */     
/* 343 */     if (page > 1) {
/* 344 */       gui.setItem(45, TagGUI.createItem(Material.PAPER, (short)0, 1, "Back to page " + (page - 1), null));
/*     */     }
/*     */     
/* 347 */     if (page < pages) {
/* 348 */       gui.setItem(53, TagGUI.createItem(Material.PAPER, (short)0, 1, "Forward to page " + (page + 1), null));
/*     */     }
/*     */     
/* 351 */     gui.openInventory(p);
/* 352 */     return true;
/*     */   }
/*     */ }


/* Location:              D:\work\DeluxeTags-1.8.1-Release_2.jar!\me\clip\deluxetags\gui\GUIHandler.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */